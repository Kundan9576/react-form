const axios = require('axios');

let addUserApi = () => {

    axios.post('https://jsonplaceholder.typicode.com/posts', {

        body: JSON.stringify({
            title: 'foo',
            body: 'bar',
            userId: 1,
        })

    })
        .then((response) => {
            let responseData = response.data;
            console.log(responseData);
        })
        .catch((error) => {
            console.log(error);
        });

}

export default addUserApi;