import React, { useState } from 'react';
import UpdateUserForm from '../newForm/EditForm';

let TableList = ( props ) => {
    let initalvalue = {
        id: '',
        name: '',
        email: ""
      }
    const [items, setItems] = useState( initalvalue );
    let userDataList = props.items;
    let counter =  0;

    let edit = ( data ) => {
        console.table( data );
        setItems( data );
    }
    let deleteDetails = ( data ) => {
        console.log( data )
    }

    return (
        <React.Fragment>
            <div className="new-expense">
                <UpdateUserForm items={ items }/>
            </div>
            <div className="new-expense2">
                <table style={{ textAlign: 'center' }} className="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Sl No.</th>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            userDataList.map(
                                (userData) => {
                                    // console.log( userData )
                                    counter++;

                                    return <tr key={ userData.id }>
                                                <th scope="row">{ counter }</th>
                                                <td>{ userData.id }</td>
                                                <td>{ userData.name }</td>
                                                <td>{ userData.email }</td>
                                                <td>
                                                    <div>
                                                        <button className="btn btn-success" onClick={ () => edit( userData ) } >Edit</button>
                                                        <button style={{ marginLeft: '30px' }} className="btn btn-danger" onClick={ () => deleteDetails( userData ) } >Delete</button>
                                                    </div>
                                                </td>
                                           </tr>
                                    
                                }
                            )
                        }
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    )
}

export default TableList;