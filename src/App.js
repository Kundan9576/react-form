import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import NewUser from './newForm/NewUser';
import TableList from './components/TableList';
import addUserApi from './apis/axios';

const dummy_data = [
  {
    id: 'e1',
    name: 'abc',
    email: "abc@gmail.com"
  },
  {
    id: 'e2',
    name: 'def',
    email: "def@gmail.com"
  },
  {
    id: 'e3',
    name: 'mnp',
    email: "mnp@gmail.com"
  },
  {
    id: 'e4',
    name: 'xyz',
    email: "xyz@gmail.com"
  }
];

const App = () => {
 
  const [ datas, setDatas ] = useState( dummy_data );  
  addUserApi();
  let addExpense = ( data ) => {
    setDatas((prevData) => {
      return [data, ...prevData];
    });
    console.log( data )
  } // from this we have done child to parent binding.

  return (
    <div>
      <NewUser onAddExpense ={ addExpense }/>
      <TableList items={ datas } />
    </div>
  );
}

export default App;
