import React, { useState } from 'react';
import './NewUser.css';

import AddUserForm from './AddUserForm';

const NewUser = ( props  ) => {

  const [isEditing, setIsEditing] = useState(false);

    const saveUserDataHandler = ( enteredUserData ) => {
        const userData = {
          ...enteredUserData,
          id: Math.random().toString()
        };
        console.table( userData )
        props.onAddExpense( userData );
        setIsEditing(false);
      };

      const startEditingHandler = () => {
        setIsEditing(true);
      };
    
      const stopEditingHandler = () => {
        setIsEditing(false);
      };

    return <div className="new-expense">

      {/* {isEditing ? <AddUserForm onSaveExpenseData={ saveUserDataHandler } onCancel={stopEditingHandler} /> : <button onClick={startEditingHandler}>Add New Expense</button> } */}
      
      {/* both are same thing with 2 different methods */}

      { !isEditing && ( <button onClick={ startEditingHandler }>Add New User</button> ) }
      { isEditing && ( <AddUserForm onSaveExpenseData={ saveUserDataHandler } onCancel={stopEditingHandler} /> ) }    

    </div>
}

export default NewUser;