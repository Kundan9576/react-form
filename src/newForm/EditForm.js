import React, { useState, useEffect } from 'react';
import './EditForm.css';

const UpdateUserForm = ( props ) => {

    let data = props.items;

    const [id, setId] = useState( data.id );
    const [name, setName] = useState( data.name );
    const [email, setEmail] = useState( data.email );
    const [password, setPassword] = useState('');

    useEffect(()=>{
        setId( data.id );
        setName( data.name );
        setEmail( data.email );
    },[ props ]);

    let clear = () =>{
        data =  {
            id: '',
            name: '',
            email: ""
          }
    }



    let idChangedhandler = ( event ) => {
        setId( event.target.value );
    }
    let nameChangedhandler = ( event ) => {
        setName( event.target.value );
    }

    let emailChangeHandler = ( event ) => {
        setEmail( event.target.value );
    }

    let passwordChangedhandler = ( event ) => {
        setPassword( event.target.value );
    }

    let submit = ( event ) => {
        event.preventDefault();
        let data = {
            id: id,
            name: name,
            email: email,
            password: password
        }
        console.log( "padte Value", data );        
    }

    return (
        <React.Fragment>
            <h4 style={{ fontWeight: "700"}}>Edit User Details</h4>
        <form>
            <div className="new-expense__controls">

                <div className = "new-expense__control">
                    <label>ID</label>
                    <input type="text" value={ id } onChange = { idChangedhandler } />
                </div>

                <div className = "new-expense__control">
                    <label>Name</label>
                    <input type="text" value={ name } onChange = { nameChangedhandler } />
                </div>

                <div className = "new-expense__control">
                    <label>Email</label>
                    <input type="text" value={ email } onChange = { emailChangeHandler } />
                </div>

                <div className = "new-expense__control">
                    <label>Password</label>
                    <input type="password" value={ password } onChange = { passwordChangedhandler } />
                </div>

                <div className="new-expense__actions add-btn">
                    <button type="button">Cancel</button>
                    <button type="button" onClick={ submit } >update User</button>
                    <button type="button" onClick={ clear }>Clear</button>
                </div>

            </div>
        </form>
        </React.Fragment>
    );
}

export default UpdateUserForm;