import React, { useState } from 'react';
import './AddUserForm.css';

const AddUserForm = ( props ) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    let nameChangedhandler = ( event ) => {
        setName( event.target.value );
    }

    let emailChangeHandler = ( event ) => {
        setEmail( event.target.value );
    }

    let passwordChangedhandler = ( event ) => {
        setPassword( event.target.value );
    }

    let submit = ( event ) => {
        event.preventDefault();
        if( name == '' || email == '' || password == ''){
            alert("Please enter the desired Inputs.");
        } else{
            let enteredData = {
                name: name,
                email: email,
                password: password
            }
            props.onSaveExpenseData(enteredData);
            setName('');
            setEmail('');
            setPassword('');
        }
        
    }

    return (
        <form>
            <div className="new-expense__controls">

                <div className = "new-expense__control">
                    <label>Name</label>
                    <input type="text" value={ name } onChange = { nameChangedhandler } />
                </div>

                <div className = "new-expense__control">
                    <label>Email</label>
                    <input type="text" value={ email } onChange = { emailChangeHandler } />
                </div>

                <div className = "new-expense__control">
                    <label>Password</label>
                    <input type="password" value={ password } onChange = { passwordChangedhandler } />
                </div>

                <div className="new-expense__actions add-btn">
                    <button type="button" onClick={props.onCancel}>Cancel</button>
                    <button type="submit" onClick={ submit }>Add User</button>
                </div>

            </div>
        </form>
    );
}

export default AddUserForm;